#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>

#define MAXINPUTSIZE 128
int main(int argc, char **argv)
{
    char* prompt = "308sh> ";
    char input[MAXINPUTSIZE];
    char dir[MAXINPUTSIZE];
    char cwd[1024];
    int i;
    int status;
    int countArgs;
    int child1;
    int child2;
    int backgroundProcess = 0;

    //Check if the argument is valid 
    if(argc!=3&&argc!=1){
        printf("Invalid command. Exiting. \n");
        return 0;
    }

    //check for prompt 
    if(argc == 3){
        //The first argument is not a -p, then exit if so then the prompt will be the next command that was placed
        if((strlen(argv[1]) != 2) || (strncmp("-p", argv[1], 2) != 0)){
            printf("Invalid command. Exiting. \n");
            return 0;
        }
        prompt = argv[2];
    }
    //inf loop that acts as the shell 
    while(1){
        printf(prompt);
        fgets(input, MAXINPUTSIZE, stdin);

        if((strlen(input) > 0) && (input[strlen(input)-1] == '\n')){
            input[strlen(input)-1] = '\0';
        }

        //built in commands
        //exit 
        if(strcmp(input, "exit") == 0){
            return 0;
        }

        //pid
        else if(strcmp(input, "pid") == 0){
            printf("Process ID: %d\n", getpid());
        }
        //ppid
        else if(strcmp(input, "ppid") == 0){
            printf("Process ID of this shell's parent: %d\n", getppid());
        }
        //cd 
        else if(strcmp(input, "cd") == 0 && (strlen(input) == 2) || input[2] == ' '){
            if(strlen(input) == 2){
                chdir(getenv("HOME"));
            }
            else{
                for(i=0; i<strlen(input)-3;i++)
                {
                    dir[i] = input[i+3];
                    dir[i+1] = '\0';
                }
                if(chdir(dir) == -1)
                {
                    printf("Cannot find directory\n");
                }
            }
        }

        //pwd
        else if(strcmp(input, "pwd")){
            if(getcwd(cwd, 1024) == "\0"){
                printf("Faill on pwd \n");
            }
            else{
                printf("%s\n", cwd);
            }
        }
        //non built in commands
        else{
            countArgs = 1;
            for(i = 0; i<strlen(input); i++){
                //if there is a space then the amount of arguments goes up 
                if(input[i] == ' '){
                    countArgs++;
                }
            }
            //if there is a &, then background process will be enabled and the amount of arguments will go down 
            if(input[strlen(input) - 1] == '&'){
                countArgs--;
                backgroundProcess = 1;
            }
            char temp[countArgs][MAXINPUTSIZE];
            char* args[countArgs + 1];
            countArgs = 0;
            int tempVal = 0;
            //Loop to go through the user's input and fill the argument array with them 
            for(i = 0; i< strlen(input) + 1; i++){
                //if the input is a space or a null char, place the value in a temp array letting it know that there is no 
                //argument in that
                if(input[i] == ' ' || input[i] == '\0'){
                    temp[countArgs][tempVal] = '\0';
                    args[countArgs] = temp[countArgs];
                    countArgs++;
                    tempVal = 0;
                }
                else if(input[i] == '&' && i == strlen(input) - 1){
                    //if the input is an & and is the last argument, then increase the increment value up to end the loop
                    i = strlen(input) + 1;
                }
                else{
                    //if the value is another arugment, put it in the temp array and then increment the tempVal
                    temp[countArgs][tempVal] = input[i];
                    tempVal++;
                }
            }
            args[countArgs] = (char*) NULL;
            //if a background process is called, the child will run in the background, but it will not wait until for
            //the 2nd child being done, it will wait until the first child being done which means you could run another command in that time 
            if(backgroundProcess == 1){
                backgroundProcess = 0;
                child2 = fork();
                if(child2 == 0){
                    child1 = fork();
                    if(child1 == 0){
                        //print process ID and name of command
                        printf("[%d] %s\n", getpid(), args[0]);
                        execvp(args[0], args);
                        perror("\0");
                        return 0;                        
                    }
                    else{
                        status = -1;
                        waitpid(-1, &status, 0);
                        printf("\n[%d] %s Exit %d\n", child1, args[0], WEXITSTATUS(status));
                        printf(prompt);
                        return 0;
                    }
                }
                else{
                    usleep(1000);
                }
                
            }

        else{
            //if its not in a background process, a child will get made and and wait until the child ran successfully then it will exit
            child1 = fork();
            //in child process
            if(child1 == 0){
                //print process ID and name of command
                printf("[%d] %s\n", getpid(), args[0]);
                execvp(args[0], args);
                perror("\0");
                return 0;
                }
            else{
                usleep(1000);
                status = -1;
                waitpid(child1, &status, 0);
                //print the completed child process ID name and return status
                printf("\n[%d] %s Exit %d\n", child1, args[0], WEXITSTATUS(status));
                }
            }
        }
    }
    return 0;
}