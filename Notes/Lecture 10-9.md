
## Translation Lookaside Buffer (TLB)
- A hardware cache of popular virtual-to-physical address translation
	- Purpose : to speedup paging
	- Location : part of the memory-management unit (MMU)

Structure
- Associate registers - parallel search
- Address translation (Virtual Page#, Physical Frame#)
	- If virtual page# is in associative register
		- get the corresponding physical frame# from TLB
	- Otherwise
		- Walk through page table to get the frame#
- Each entry may also contain other bits
	- E.g., ASID: address space ID

Effective Access Time (EAT)
- effective time for accessing memory with TLB
- TLB Hit ratio = $\alpha$
	- percentage of times that a page number is found in the TLB
	- TLB hit : one memory access
	- TLB miss : two memory access
- Consider $\alpha$ = 80%, 100 ns for each memory access
	- EAT = 0.80 x 100 + 0.20 x 200 = 120ns
- Consider a more realistic hit ration $\alpha$ = 


The TLB improves performance due to spatial locality 


Locality 
Temporal Locality 
- An instruction or data item that has been recently 



Each process needs to have a page table 
- 100 processes needs 4MB * 100 = 400 MB


Paging the (linear) page table
- Chop up the page table into page-sized units
- If an entire page of page-table entries is invalid, don't allocate that page of the page table 

