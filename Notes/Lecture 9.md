
Draw a Gantt Chart for the scheduling algorithms 

![[Pasted image 20230911090638.png]]

Average waiting time = [(10-1)+(1-1)+(17-2)+(5-3)]/4


Round Robin 
- Each process is assigned a time interval (quantum/quota) during which it is allowed to run 


| $P_1$ | $P_2$ | $P_3$ | $P_1$ | $P_1$ | $P_1$ | $P_1$ | $P_1$ |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
0    4      7     10    14    18    22    26   30


Priority Scheduling 
- A priority number is associated with each process







