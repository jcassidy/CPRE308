
- Deadlock
	- two or more processes are waiting indefinitely for an event that can be caused by only one of the waiting processes
	- Eg. Let S and Q be two semaphores init to 1

$P_0$  
down(S);
down(Q);
...
up(S);
up(Q);

$P_1$
down(Q);
down(S);
...
up(Q);
up(S);

- Comparison with semaphore

- Comparison with mutex lock

#### Conditional Variables
![[Pasted image 20230918092705.png]]


When using condition variables there is always a Boolean predicate involving shared variables associated with each condition wait that is true if the thread should proceed


Classic Synchronization Problem
- Philosopher
	- eat, think
	- eat, think
	- .......

- Philosopher = Process
- Eating needs two resources (chopsticks )