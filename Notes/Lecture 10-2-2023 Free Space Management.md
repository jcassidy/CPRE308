
- Link free chunks
- Tracking the size of allocated regions 
	- The C library function **void *malloc(size_t size)** requires a size as input, and allocates the requested memory and returns a pointer to it
	- The header minimally contains the size of the allocated memory region
	- The header may also contain other info
		- e.g., a magic number for integrity checking

- Simple pointer arithmetic to find the header point
- Linked List
	- Memory allocation algorithms
		- Best Fit
			- Find free chunks that are at least as big as the request
			- Return the smallest one in the group of candidates
		- Worst Fit
			- Find the largest free chunks and allocate the amount of the request (so that the remaining chunk may still be usable)
			- Keep the remaining chunk on the free list
		- First Fit
			- Find the first chunk that is big enough for the request
			- Return the requested amount and keep the remaining chunk on the free list 


Segregated List
- Keep free chunks of different sizes in a segregated list for popular requests
- New Complication
	- How much memory should dedicate to the pool of memory that serves specialized requests of a given size
- Slab allocator handles this issue
	- Allocate an initial number of object caches
		- The objects are likely to be requested frequently
			- e.g. locks
	- When a given cache is running low on free space, request more memory from a more general memory allocator 

Buddy Allocation
- The allocator divides free space by two until a block that is just big enough to accommodate the request is found 
- Buddy allocation can suffer from internal fragmentation
- Buddy system makes coalescing simple
	- Coalescing two blocks into the next level of blocks 
