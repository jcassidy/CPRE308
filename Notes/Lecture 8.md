
#### Process Scheduling

- When to run a different process
	- New process is created
	- Process terminates
	- A process blocks on I/O etc
	- interrupt occurs
- Which one to run
	- Depends on system characteristics/requirements
		- Various scheduling algorithms
- Process behavior
	- CPU-Bound 
	- I/O-Bound

##### Scheduling Concept
- Non-preemptive V.S. preemptive scheduling
	- Non-preemptive
		- Processes run until they are blocked (for I/O) or voluntarily releases the CPU
	- Preemptive
		- Can forcibly suspend a process and switch to another

- Goals of scheduling algorithms
	- General goals
		- Fairness
			- giving each process a fair share of the CPU
		- Policy enforcement
			- ensuring that desired policy is carried out
		- Balance
			- keeping all parts of the system busy
		- Throughput
			- maximize jobs per hour
		- Turnaround time
			- minimize time between submission and termination
		- CPU Utilization
			- keep the CPU busy all the time 
		- Response time
			- Respond to user requests quickly
		- Proportionality
		- Meeting deadlines
			- hard real time
				- there are absolute deadlines that must be met
			- soft real time
				- missing an occasional deadline is undesirable, but nevertheless tolerable 
- Different systems may have different specific goals 

##### Scheduling Algorithms

First-Come, First Serve (FCFS)
	- Scheduling based on the arrival orders of processes
Shortest-Job-First
	- Scheduling based on (predicted) CPU times
