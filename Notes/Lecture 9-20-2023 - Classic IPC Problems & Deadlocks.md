
Semaphore - integer variable 
	two indivisible (atomic ) operations
		down() and up()
		P() and V()
		wait() and signal()

Conditional Variables
- allows a thread to wait till a condition is satisfied
- Testing the condition must be done within a mutex
- A mutex is associated with every condition variable


![[Pasted image 20230918092705.png]]





## Dining Philosophers Problem 

First Attempt 
```
Philosopher i:

while(1) {
	Think();
	lock(Left_Chopstick);
	lock(Right_Chopstick);
	Eat();
	unlock(Left_Chopstick);
	unlock(Right_Chopstick);
}
```

Second Attempt
```
Philosopher i:

Think();
unsuccessful = 1;
while(unsuccessful){
	lock(left_chopstick);
	if(0==trylock(right_chopstick))
		unsuccessful =0;
	else
		unlock(left_chopstick)
}
Eat();

unlock(left_chopstick);
unlock(right_chopstick);
```


Solutions without Random Delays
- Do not try to take chopsticks one after another
- Try to grab both chopsticks at the same time
- Or try to grab the entire table 


```
Philosopher i:
	lock(table);
	Eat();
	Unlock(table);
```


## Readers-Writers Problem

Problem Scenario
- Multiple threads reading from/writing to a shared resource
- Many threads can read simultaneously
- Only one can be writing at any time 

Solution Idea
- Readers 
	- First readers lock the database
	- If a reader is inside the critical region, other readers may enter the critical region without locking again
	- Checking & updating for the reader count is protected by the mutex lock
- Writers
	- Always need to lock up database before entering 

![[Pasted image 20230920092031.png]]

Potential problem
- Writer starvation 
	- Readers might continuously enter while a writer waits  

Other variants possible 


## Deadlock Concepts

- What's a deadlock 
	- A set of processes/threads is deadlocked if each process/thread in the set is waiting for an event that only another process/thread in the set can cause
- Why do deadlocks occur
	- Many reasons
		- In large code bases, complex dependencies arise between component

- Deadlock solutions
	- Deadlock detection & recovery
		- Allow deadlock to occasionally occur and then take some action 
			- E.g if an OS froze, you would reboot it 
		- Many database systems employ deadlock detection and recovery technique
