
### Memory Hierarchy
- Diverse technologies with tradeoffs
	- Latency, capacity, persistency, cost, ...
	- non-volatile memories are revolutionizing the market
		- A "disruptive" technology

### Memory Management in Early Systems
- Load only one program in memory
	- poor utilization/efficiency

- Multiprogramming
	- Execute each for a while
	- Switch processes between them in memory
	- Increase utilization and efficiency 

	- Memory allocation changes as processes come and leave
	- Relocation using a pair of base and limit registers
	- Basic protection via base/limit registers
		- CPU checks every memory address generated in user program to be sure it is between base and limit for that user

### Virtual Memory 
- Modern OS provides a virtual memory for each process
	- Provides the illusion that each process uses the whole memory itself
	- Can handle programs that are too large to fit in physical memory 
- Benefits
	- Memory efficiency
	- Isolation & Protection among processes

### Address Translation
- Translate virtual address to physical address
- Static address translation
	- Map virtual address to physical address when loading a process into memory
	- Used when processes are fixed and known in advance
		- e.g many embedded systems
  - Dynamic address translation
	- Map virtual address to physical address at runtime