
The page table for each process is stored in physical memory 


### What is inside a page table 

- The page table is just a data structure that is used to map the virtual address to physical address
- The OS indexes the array by VPN, and looks up the page table entry
	- Each entry must contain the base address of each page in the physical memory
	- May also contain address

Valid Bit - Indicating whether the particular translation is valid
Protection Bit - Indicating whether the page could be read from, written to, or executed from
Present Bit - Indicating whether the page has been modified since it was brought into memory
Reference Bit - Indicating that a page has been accessed

### Shared Pages
- One page in the physical memory is mapped to the address spaces of multiple processes 
- Also useful for inter-process communication if sharing of read-write is allowed


### Potential Issues of Paging 
- Time
	- For every user memory access, one additional memory access is needed 
- Space
	- Page tables can get awfully large
		- 32 bit address space (4GB) with 4KB pages
		- Each process needs to have a page table 