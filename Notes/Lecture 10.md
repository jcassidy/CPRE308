

### Basic Concepts 

Processes within a system may be independent or cooperating 
- Example reasons for cooperating
	- Information sharing 
	- Computation speedup 
- Cooperating processes need inter-process communication mechanism
	- Communicate with each other
	- Ensure 
		- (1) do not get in each other's way
		- (2) proper ordering when dependencies are present 
	- Violating the two may lead to "concurrency bugs"

Two basic methods of IPC
- Message passing
- Shared memory

Race Condition 
- Two or more processes are reading/writing the same shared data and the final result depends on who runs precisely when 
	- At least one access is write 
- E.g. , printer with two processes
	- Two shared variables

Two threads perform "counter = counter + 1"
- "counter" is a shared variable
- initially counter = 50 (stored at address 0x8049a1c)

Critical Region
- A piece of code that accesses a shared variable and must not be concurrently executed by more than one thread
	- Multiple threads executing critical section can result in a race condition
	- Need to support mutual execution 

Solutions of Mutual Exclusion 

- Disabling Interrupts 
	- Disable interrupts when a process is in critical region 
	- Cons 
		- Not safe
			- What if a process does not turn them on
			- More suitable for kernel instead of user processes
		- Not applicable for multiple processors 
			- Processes on other processors may still access shared variables 
	- Less used today

- Strict Alternation
	- Use a variable to keep track of whose turn it is to enter the critical region 

- Cons 
	- Busy waiting (continuously testing a variable until some value appears) wastes CPU cycles
		- A lock that uses busy waiting is called a spin lock 

