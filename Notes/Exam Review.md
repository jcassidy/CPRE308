
- Slides would be the most important materials 

#### Why OS
- Fundamental to computer systems
- Fundamental to modern society
- OS is a resource manager
- OS is a control program 

#### Introduction 
- OS Abstractions for HW
	- CPU
		- process and/or thread
	- Memory
		- address space

#### OS Structures
- Monolithic kernel VS Microkernel
	- Monolithic : the OS runs as a single program in kernel mode
	- Microkernel : split the OS up into small, well-defined modules, only one of which - the microkernel - runs in kernel mode

#### Process
- A program in execution
- holds all the info needed to run a program

#### Process Address Space
- Four segments
	- Stack
		- local variables, function parameters, return address
	- Heap
		- dynamically allocated data
	- Static data
		- global/static variables
	- Code (text)
		- instructions of the program


#### Process States
- Three basic states
	- Running 
	- Ready 
	- Blocked
- Other states (e.g Zombie)

#### Process Context 
- stored in a "process control block"
- Context Switch
	- Switching the CPU to another process by 
		- saving the context of an old process
		- loading the context of a new process

#### Fork()
- Some times the results are non-deterministic 

#### Multi-threaded Process
- Multiple threads of control within a process

#### Process/Thread Scheduling
- First-Come, First Served (FCFS)
	- Scheduling based on the arrival order of process
![[Pasted image 20230922091419.png]]

![[Pasted image 20230922091454.png]]

![[Pasted image 20230922091514.png]]
![[Pasted image 20230922091532.png]]
![[Pasted image 20230922091546.png]]

#### Inter-Process Communication
![[Pasted image 20230922092034.png]]

#### Solutions of mutual exclusion
- Pthread locks


#### Real world Concurrency Bugs 
- Data race
- atomicity violation 
- order violation 
- deadlock
	- Resource allocation graph 

#### Deadlock conditions
- Four conditions need to hold for deadlock to occur 



- Exam will be on paper
- Examples will be on the slide 




**( ) 1. OS is responsible for managing the hardware resources in a computer.**  
True
**( ) 2. Linux is one of the most widely used Microkernel OS.**  
False
**( ) 3. The heap region of a process’ address space is automatically allocated and deallocated.**  
False
**( ) 4. System call is used by the hardware to communicate with the OS.**  
False
**( ) 5. Context-switch time is overhead.**  
True
**( ) 6. OS controls the execution of programs to prevent errors and improper use of the computer.**  
True
**( ) 7. Monolithic kernel may have higher performance than Microkernel in practice because it incurs less  communication overhead among modules.**  
True
**( ) 8. Type 1 hypervisor requires support from an underlying OS.**  
False
**( ) 9. The malloc() library call in C dynamically allocates memory in the stack region.**
False

**What is a process? What is a "Zombie" process?**

A process is program in execution, it is very different from a program. while a program is static code and static data compared to a process which is a dynamic instance of code and data. You could have multiple processes of the same program

A zombie process is a process that has completed execution but still has an entry in the process table. It allows the parent process to read its child's exit status and once the exit status is read (via the wait system call), the zombie's entry is removed from the process table and it said to be "reaped"

**Compare the advantage & disadvantage of Monolithic kernel and Microkernel**

Advantage of Microkernel
- Several modules may be modified, reloaded, replaced without modifying the kernel
- The architecture is small and isolated, but it may worked better
- The system could be expanded more easily because it may be added to the system application without interrupting the kernel
- It adds new features without recompiling
Disadvantage of Microkernel
- A context switch is required in the microkernel when the drivers are run as processes
- The microkernel system performance might be variable and cause issues
- Microkernel services are more expensive than in a traditional monolithic system 
Advantages of Monolithic Kernel
- The monolithic kernel runs quickly because of memory management, file management, process scheduling
- All of the components may interact directly with each other's and also with the kernel
- It is a huge process that executes completely within a single address space
- Its structures are easy and simple. The kernel contains all of the components required for processing 
Disadvantage of Monolithic Kernel
- If the use needs to add a new service, the user requires to modify the complete operating system 
- It isn't easy to port code written in the monolithic system
- If any of the services fail, the entire system fails 

![[Pasted image 20230924200341.png]]

4 process are created from this code. 