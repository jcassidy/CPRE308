
Student Name : Jack Cassidy 
University-ID: 164190050


#### Problem 1 - True or False
1. True 
2. False
3. False 
4. False
5. True
6. True
7. True
8. False
9. False

#### Problem 2 
##### What is a process? What is a "Zombie" process?

A process is a program in execution, it is very different from a program, while a program is static code and static data compared to a process which is a dynamic instance of code and data. You could have multiple processes of the same program. 

A zombie process is a process that has completed execution but still has an entry in the process table. It allows the parent process to read its child's exit status and once the exit status is read (via the wait system call), the zombie's entry is removed from the process table and it said to be "reaped"


#### Problem 3

##### Compare the advantage & disadvantage of Monolithic kernel and Microkernel 

Advantages of Micro Kernel 
* Several modules may be modified, reloaded, replaced without modifying the kernel
* The architecture is small and isolated, but it may worked better
* The system could be expanded more easily because it may be added to the system application without interrupting the kernel
* It adds news features with out recompiling

Disadvantages of Micro Kernel
* A context switch is required in the microkernel when the drivers are run as processes 
* The microkernel system performance might be variable and cause issues
* Micokernel services are more expensive than in a traditional monolithic system

Advantages of Monolithic Kernel
* The monolithic kernel runs quickly because of memory management, file management, process scheduling
* All of the components may interact directly with each other's and also with the kernel
* It is a huge process that executes completely within a single address space
* Its structures are easy and simple. The kernel contains all of the components required for processing 

Disadvantage of Monolithic Kernel 
- If the user needs to add a new service, the user requires to modify the complete operating system
- It isn't easy to port code written in the monolithic operating system
- If any of the services fail, the entire system fails 

#### Problem 4

##### What is the maximum depth of the stack for the following code in terms of number of stack frames (one stack frame per function call) Be sure to count the first call to main

```c
int main(){
	f(12);
	return 1;
}

int f(int n){
	if (n <= 0)
		return 0;
	else 
		return f(n-1)+2*f(n-4)
}
```

```mermaid
graph TD;
12 --> 11A;
12 --> 8B;
11A --> 10A;
11A --> 7;
10A --> 9A;
10A --> 6;
9A --> 8A;
9A --> 5;
8A --> 7A;
8A --> 4;
7A --> 6A;
7A --> 3;
6A --> 5A;
6A --> 2;
5A --> 4A;
5A --> 1;
4A --> 3A;
4A --> 0;
3A --> 2A;
2A --> 1A;
1A --> 0A;

8B --> 7B;
8B --> 4C;
7B --> 6B;
7B --> 3C;
6B --> 5B;
6B --> 2C;
5B --> 4B;
5B --> 1C;
4B --> 3B;
4B --> 0B;
3B --> 2B;
2B --> 1B;
1B --> 0C;
```

For every calls, the function call its 2 times so using that we could figure out the number of stack frames by doing 2^13 which equals 8192


#### Problem 5

##### 1. How many processes does the following code create? 2. Draw the process tree of the program


```c
int main(){
	int i;
	for(i = 1; i<3; i++)
		fork();
	return 1; 
}
```

7 process are created from this code.

Process Tree
```mermaid
graph TD;
1 --> 2;
1 --> 3;
2 --> 4;
2 --> 5;
3 --> 6;
3 --> 7;
```


#### Problem 6

##### Please write all possible outputs from the following piece of code

```c
int main(void){
	pid_t pid = fork();
	if (pid > 0) {
		printf("I am the parent\n")
	} else if (pid == 0){
		printf("I am the child\n")
	}
	else printf("ERROR!\n")
	return 0;
}
```

From this there are two possible outputs, the easy one is if the fork() fails then it would output
**ERROR**!
, the other one is if it succeeds then it would print out 
**I am the parent**
**I am the child**


#### Problem 7
Consider the following code. Assume all system calls return successfully and the actual process IDs of the parents and child during the execution are 2600 and 2603 , respectively. What are the values of pid/pid1 at lines A, B, C, D?

```c
int main() { 
	pid_t pid, pid1; 
	pid = fork(); 
	if (pid < 0) { 
		fprintf(stderr, “Fork Failed”);
		 return 1;
		} else if (pid == 0){ 
			pid1 = getpid(); 
			printf(“child: pid = %d”, pid); /* A */ 
			printf(“child: pid1 = %d”, pid1); /* B */ 
		} 
		} else { 
			pid1 = getpid(); 
			printf(“parent: pid = %d”, pid); /* C */ 
			printf(“parent: pid1 = %d”, pid1); /* D */ 
			wait(NULL); 
			} 
			return 0; 
	}
```


Line A of pid is 0
Line B of pid1 is 2603
Line C of pid is 2603
Line D of pid is 2600