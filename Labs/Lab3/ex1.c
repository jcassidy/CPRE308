#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* thread1();
void* thread2();

int main(){

    //Function prototypes
    pthread_t i1;
    pthread_t i2;

    sleep(5);
    //create both threads i1 and i2
    pthread_create(&i1, NULL, thread1, NULL);
    pthread_create(&i2, NULL, thread2, NULL);
    //wait for threads to complete
    pthread_join(i1, NULL);
    pthread_join(i2, NULL);

    printf("Hello from the main thread\n");
    return 0;
}

void * thread1(){
    printf("Hello from thread 1\n");
    return NULL;
}

void * thread2(){
    printf("Hello from thread 2\n");
    return NULL;
}